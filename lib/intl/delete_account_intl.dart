class DeleteAccountIntl {

  static String title = "Delete Account";

  static String intro = "If you delete your account, the following will be <red>deleted</red> and <red>cannot be recovered</red>: ";

  static String rule = """
•	All of your personal information
•	All of your remaining Coins and Bonus balance
•	All of your unlocked chapters
•	All of your reading history information
""";

  static String unlimited = "If you  have an <bold>UNLIMITED</bold> subscription, please manually cancel it at the Apple Store before renewing.";
  static String remarks = "To confirm that you agree to the permanent deletion of your account and all associated information, you may press the <red>Delete My Account</red> button below.";

  static String delete = "Delete My Account";


  // cancel delete account
  static String cancel_title = "Application";
  static String apply_account = "Apply Account";
  static String apply_reason = "Apply Reason";
  static String reason_placeholder = "Please enter the reason for the appliction of withdraw Delete Account ";
  static String submit = "Submit";
}