
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:styled_text/styled_text.dart';

import '../intl/delete_account_intl.dart';
import '../alert/delete_account_alert.dart';
import 'delete_account_page_store.dart';

// 删除账号
//     参数：
//       "onSuccess": () async {}
//       "onLogoff": () async {},
//       "primary": const Color(0xFF553EFF)
class DeleteAccountPage extends StatefulWidget {

  final BuildContext? context;

  final double? scale; // 字体放大比例

  final Color? primary;
  final Color? textColor;

  final Future<void> Function(void Function(BuildContext))? onSuccess;
  final Future<bool> Function(void Function(BuildContext))? onLogoff;

  const DeleteAccountPage({Key? key, this.context, this.onSuccess, this.onLogoff, this.scale, this.primary, this.textColor}) : super(key: key);

  @override
  _DeleteAccountPageState createState() => _DeleteAccountPageState();
}

class _DeleteAccountPageState extends State<DeleteAccountPage> {

  late DeleteAccountPageStore controller;

  @override
  void initState() {
    controller = DeleteAccountPageStore(context, widget.onSuccess, widget.onLogoff);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          elevation: 0.5,
          title: Text(
              DeleteAccountIntl.title,
              style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
              )
          ),
          systemOverlayStyle: SystemUiOverlayStyle.dark,
          leading: GestureDetector(
            child: const Icon(
                Icons.arrow_back_outlined),
            onTap: () =>
                Navigator.of(context).pop(),
          ),
        ),
        body: Column(
          children: [
            Expanded(child: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                    child: Container(
                      height: 20,
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 18),
                      child: StyledText(
                          text: DeleteAccountIntl.intro,
                          style: const TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFF15141F),
                          ),
                          tags: {
                            "red": StyledTextTag(
                                style: const TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  color: Colors.red,
                                )
                            )
                          }
                      )
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                      height: 12,
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 18),
                      child: Text(
                          DeleteAccountIntl.rule,
                          style: const TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFF15141F),
                          )
                      ),
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                      height: 32,
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 18),
                        child: StyledText(
                            text: DeleteAccountIntl.unlimited,
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Color(0xFF15141F),
                            ),
                            tags: {
                              "bold": StyledTextTag(
                                  style: const TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xFF15141F),
                                  )
                              )
                            }
                        )
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                      height: 32,
                    )
                ),
                SliverToBoxAdapter(
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 18),
                        child: StyledText(
                            text: DeleteAccountIntl.remarks,
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.normal,
                              color: Color(0xFF15141F),
                            ),
                            tags: {
                              "red": StyledTextTag(
                                  style: const TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.red,
                                  )
                              )
                            }
                        )
                    )
                ),
              ],
            )),
            GestureDetector(
              onTap: () {
                showLogoffConfirmAlert(
                  context,
                  onDelete: controller.logoff
                );
              },
              child: Container(
                  width: MediaQuery.of(context).size.width - 36,
                  height: 48,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: widget.primary,
                      borderRadius: BorderRadius.circular(48),
                  ),
                  child: Text(
                      DeleteAccountIntl.delete,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: widget.textColor ?? Colors.white,
                      ),
                      textAlign: TextAlign.center)
              ),
            ),
            Container(
              height: MediaQuery.of(context).padding.bottom + 15
            )
          ],
        )
    );
  }
}
