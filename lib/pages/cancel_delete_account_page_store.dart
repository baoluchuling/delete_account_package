///MePageStore
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CancelDeleteAccountPageStore with ChangeNotifier {

  final BuildContext context;
  final Future<bool> Function(String?)? onCancelLogoff;

  CancelDeleteAccountPageStore(this.context, this.onCancelLogoff);

  void cancelLogoff() async {
    if (reasonValue == null) {
      notifyListeners();
      return;
    }
    if (reasonValue!.replaceAll(new RegExp(r"\s+\b|\b\s"), "").isEmpty) {
      reasonValue = null;
      notifyListeners();
      return;
    }
    if (onCancelLogoff != null && (await onCancelLogoff!(reasonValue))) {
      Navigator.of(context).pop();
    }
  }

  String? reasonValue;
  void updateReason(String? reason) async {
    reasonValue = reason;
    notifyListeners();
  }
}
