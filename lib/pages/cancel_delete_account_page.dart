
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:provider/provider.dart';

import '../intl/delete_account_intl.dart';
import 'cancel_delete_account_page_store.dart';

// 删除账号
//     参数：
//       "onSuccess": () async {}
//       "onLogoff": () async {},
//       "primary": const Color(0xFF553EFF)
class CancelDeleteAccountPage extends StatefulWidget {

  final BuildContext? context; // 字体放大比例

  final double? scale; // 字体放大比例

  final Color? primary;
  final Color? textColor;
  final String? avatar;
  final ImageProvider? placeholder;
  final String? nickName;
  final String? userId;

  final Future<bool> Function(String?)? onCancelLogoff;

  const CancelDeleteAccountPage({Key? key, this.context, this.avatar, this.placeholder, this.userId, this.nickName, this.onCancelLogoff, this.scale, this.textColor, this.primary}) : super(key: key);

  @override
  _CancelDeleteAccountPageState createState() => _CancelDeleteAccountPageState();
}

class _CancelDeleteAccountPageState extends State<CancelDeleteAccountPage> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CancelDeleteAccountPageStore(context, widget.onCancelLogoff),
      child: Scaffold(
          backgroundColor: const Color(0xFFF6F6F8),
          appBar: AppBar(
            backgroundColor: Colors.white,
            foregroundColor: Colors.black,
            elevation: 0.5,
            title: Text(
                DeleteAccountIntl.cancel_title,
                style: const TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                )
            ),
            systemOverlayStyle: SystemUiOverlayStyle.dark,
            leading: GestureDetector(
              child: const Icon(
                  Icons.close_rounded),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          body: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                        child: Container(
                          padding: const EdgeInsets.only(left: 18, right: 18, top: 20, bottom: 12),
                          child: Text(DeleteAccountIntl.apply_account, style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFF15141F),
                          )),
                        )
                    ),
                    SliverToBoxAdapter(
                        child: Container(
                            color: Colors.white,
                            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 16),
                            child: Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: Colors.grey,
                                  backgroundImage: (widget.avatar != null)
                                      ? CachedNetworkImageProvider(widget.avatar!)
                                      : widget.placeholder,
                                ),
                                const SizedBox(width: 12),
                                Expanded(child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(widget.nickName ?? "", textAlign: TextAlign.left, style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.normal,
                                      color: Color(0xFF15141F),
                                    )),
                                    Text("ID: ${widget.userId ?? ""}", textAlign: TextAlign.left, style: const TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal,
                                      color: Color(0xFF95949F),
                                    ))
                                  ],
                                )),
                                Icon(Icons.check_rounded, size: 24, color: widget.primary)
                              ],
                            )
                        )
                    ),
                    SliverToBoxAdapter(
                        child: Container(
                          padding: const EdgeInsets.only(left: 18, right: 18, top: 20, bottom: 12),
                          child: Text(DeleteAccountIntl.apply_reason, style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFF15141F),
                          )),
                        )
                    ),
                    SliverToBoxAdapter(
                        child: Container(
                          child: Column(
                              children: [
                                Consumer<CancelDeleteAccountPageStore>(
                                  builder: (context, controller, child) {
                                    return Container(
                                        padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 16),
                                        color: Colors.white,
                                        height: 260,
                                        child: TextField(
                                          cursorColor: widget.primary,
                                          onChanged: controller.updateReason,
                                          autofocus: false,
                                          maxLines: 100,
                                          decoration: InputDecoration(
                                            hintText: DeleteAccountIntl.reason_placeholder,
                                            hintStyle: const TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.normal,
                                              color: Color(0xFFCAC9CD),
                                            ),
                                            border: InputBorder.none,
                                            focusedBorder: InputBorder.none,
                                            enabledBorder: InputBorder.none,
                                            errorBorder: InputBorder.none,
                                            disabledBorder: InputBorder.none,
                                          ),
                                        )
                                    );
                                  },
                                ),
                                Consumer<CancelDeleteAccountPageStore>(
                                  builder: (context, controller, child) {
                                    if (controller.reasonValue?.isEmpty ?? false) {
                                      return const SizedBox(height: 4);
                                    }
                                    return const SizedBox(height: 0);
                                  },
                                ),
                                Consumer<CancelDeleteAccountPageStore>(
                                  builder: (context, controller, child) {
                                    if (controller.reasonValue?.isEmpty ?? false) {
                                      return Container (
                                          padding: const EdgeInsets.symmetric(horizontal: 18),
                                          child: Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(
                                                    "Please enter content",
                                                    style: const TextStyle(
                                                      fontSize: 13,
                                                      fontWeight: FontWeight.normal,
                                                      color: Color(0xFFFA3830),
                                                    ),
                                                    textAlign: TextAlign.center),
                                                const Icon(Icons.error_outline_rounded, color: Color(0xFFFA3830), size: 12,)
                                              ]
                                          )
                                      );
                                    }
                                    return const SizedBox(height: 0);
                                  },
                                ),
                              ]
                          ),
                        )
                    ),
                    SliverToBoxAdapter(
                        child: Container(
                            height: 24
                        )
                    ),
                    SliverToBoxAdapter(
                      child: Consumer<CancelDeleteAccountPageStore>(
                        builder: (context, controller, child) {
                          return GestureDetector(
                            onTap: controller.cancelLogoff,
                            child: Container(
                                width: MediaQuery.of(context).size.width - 36,
                                height: 48,
                                alignment: Alignment.center,
                                margin: const EdgeInsets.symmetric(horizontal: 18),
                                decoration: BoxDecoration(
                                  color: widget.primary?.withOpacity((controller.reasonValue?.isEmpty ?? true) ? 0.5 : 1.0),
                                  borderRadius: BorderRadius.circular(48),
                                ),
                                child: Text(
                                    DeleteAccountIntl.submit,
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: (widget.textColor ?? Colors.white).withOpacity((controller.reasonValue?.isEmpty ?? true) ? 0.5 : 1.0),
                                    ),
                                    textAlign: TextAlign.center)
                            ),
                          );
                        },
                      ),
                    ),
                    SliverToBoxAdapter(
                        child: Container(
                            height: MediaQuery.of(context).padding.bottom + 15
                        )
                    )
                  ]
              )
          ))
    );
  }
}
