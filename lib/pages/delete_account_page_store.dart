///MePageStore
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../alert/delete_account_alert.dart';

class _Throttle {

  final int delay;
  Timer? _timer;

  _Throttle({this.delay = 500});

  void call(Function callBack) {
    if (_timer != null) return;
    callBack.call();
    _timer = Timer(Duration(milliseconds: delay), () {
      _timer?.cancel();
      _timer = null;
    });
  }
}

class DeleteAccountPageStore {
  static final _Throttle _throttle = _Throttle(delay: 1000);

  final BuildContext context;
  final Future<void> Function(void Function(BuildContext))? onSuccess;
  final Future<bool> Function(void Function(BuildContext))? onLogoff;

  DeleteAccountPageStore(this.context, this.onSuccess, this.onLogoff);

  void logoff() async {
    bool result = false;
    if (onLogoff != null) {
      result = await onLogoff!((_) => {});
    }

    if (result == false) {
      return;
    }

    if (onSuccess != null) {
      await onSuccess!(_deleteAccountCallback);
    } else {
      _deleteAccountCallback(context);
    }
  }

  _deleteAccountCallback(context) {
    showLogoffSuccessAlert(context);
  }
}
