import 'package:flutter/cupertino.dart';
import '../pages/cancel_delete_account_page.dart';

Future showLogoffConfirmAlert(
    BuildContext context,
    {
      void Function()? onDelete
    }) async {
  showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: const Text('Delete Account'),
        content: const Text("Are you sure to delete your account?"),
        actions: <Widget>[
          CupertinoDialogAction(child: const Text('Cancel'), onPressed: () => Navigator.of(context).pop()),
          CupertinoDialogAction(child: const Text('Delete'), isDestructiveAction: true, onPressed: () {
            Navigator.of(context).pop();
            if (onDelete != null) {
              onDelete();
            }
          }),
        ],
      )
  );
}

Future showLogoffSuccessAlert(BuildContext context) async {
  showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: const Text('Submission Successful'),
        content: const Text("We will delete your account within 30 days. If you have subscribed to UNLIMITED, please manually cancel your UNLIMITED subscription on the Apple Store."),
        actions: <Widget>[
          CupertinoDialogAction(child: const Text('OK'), onPressed: () => Navigator.of(context).pop()),
        ],
      )
  );
}

Future showLogoffTipAlert(
    BuildContext context,
    {String? userId,
      String? nickName,
      String? avatar,
      ImageProvider? placeholder,
      Future<bool> Function(String?)? onCancelLogoff,
      Color? primary,
      Color? textColor,
    }
    ) async {
  return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: const Text('Login failed'),
        content: const Text("Your account deletion request is being processed. If you want to keep this account, please withdraw your account deletion request. "),
        actions: <Widget>[
          CupertinoDialogAction(child: const Text('Cancel'), onPressed: () async {
            Navigator.of(context).pop();
          }),
          CupertinoDialogAction(child: const Text('Withdraw'), onPressed: () async {
            Navigator.of(context).pop();
            Navigator.of(context).push(CupertinoPageRoute(
              builder: (BuildContext context) => CancelDeleteAccountPage(
                userId: userId,
                avatar: avatar,
                nickName: nickName,
                placeholder: placeholder,
                onCancelLogoff: onCancelLogoff,
                primary: primary,
                  textColor: textColor,
              ),
            ));
          }),
        ],
      )
  );
}